﻿using System;

namespace BlueTeam.Common.Extensions
{
    public static class ServiceProviderExtensions
    {
        public static TService GetService<TService>(this IServiceProvider provider)
        {
            var service = (TService)provider.GetService(typeof(TService));

            return service;
        }
    }
}