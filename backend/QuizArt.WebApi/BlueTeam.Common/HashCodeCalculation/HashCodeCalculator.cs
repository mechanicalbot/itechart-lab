﻿using System;
using System.Security.Cryptography;
using System.Text;
using BlueTeam.Common.Annotations;

namespace BlueTeam.Common.HashCodeCalculation
{
    [UsedImplicitly]
    public class HashCodeCalculator : IHashCodeCalculator
    {
        public string GetHash(string input)
        {
            using (var hashAlgorithm = new SHA256CryptoServiceProvider())
            {
                var byteValue = Encoding.UTF8.GetBytes(input);
                var byteHash = hashAlgorithm.ComputeHash(byteValue);

                return Convert.ToBase64String(byteHash);
            }
        }
    }
}