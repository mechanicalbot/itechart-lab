﻿namespace BlueTeam.Common.HashCodeCalculation
{
    public interface IHashCodeCalculator
    {
        string GetHash(string input);
    }
}