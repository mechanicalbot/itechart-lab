﻿using System;

namespace BlueTeam.Common.SystemClock
{
    public interface ISystemClock
    {
        DateTime UtcNow { get; }
    }
}