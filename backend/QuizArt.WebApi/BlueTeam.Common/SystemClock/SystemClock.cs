﻿using System;
using BlueTeam.Common.Annotations;

namespace BlueTeam.Common.SystemClock
{
    [UsedImplicitly]
    public class SystemClock : ISystemClock
    {
        public DateTime UtcNow => DateTime.UtcNow;
    }
}