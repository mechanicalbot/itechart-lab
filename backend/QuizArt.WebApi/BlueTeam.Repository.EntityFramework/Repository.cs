﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BlueTeam.Repository.EntityFramework
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly DbContext _context;
        private readonly DbSet<TEntity> _set;

        protected IQueryable<TEntity> Set => _set;


        public Repository(DbContext context)
        {
            _context = context;
            _set = context.Set<TEntity>();
        }


        public async Task<IReadOnlyCollection<TEntity>> GetAllAsync()
        {
            var surveys = await _set.ToListAsync();

            return surveys;
        }

        public async Task<TEntity> GetAsync(params object[] keyValues)
        {
            var item = await _set.FindAsync(keyValues);

            return item;
        }

        public void Create(TEntity item)
        {
            _set.Add(item);
        }

        public void Update(TEntity item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(TEntity item)
        {
            _set.Remove(item);
        }

        public async Task<int> CountAsync()
        {
            var count = await _set.CountAsync();

            return count;
        }


        protected IQueryable<TEntity> GetQuery(params Expression<Func<TEntity, object>>[] includes)
        {
            return includes.Aggregate<Expression<Func<TEntity, object>>, IQueryable<TEntity>>(_set, (current, include) => current.Include(include));
        }
    }
}