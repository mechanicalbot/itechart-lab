﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace BlueTeam.Repository.EntityFramework
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly Dictionary<Type, object> _repositories;
        private readonly Dictionary<Type, Type> _specificRepositories;

        private DbContext _context;


        protected UnitOfWork(DbContext context)
        {
            _context = context;
            _repositories = new Dictionary<Type, object>();
            _specificRepositories = new Dictionary<Type, Type>();
        }


        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : class
        {
            var type = typeof(TEntity);

            if (_repositories.ContainsKey(type))
            {
                return (IRepository<TEntity>)_repositories[typeof(TEntity)];
            }

            if (_specificRepositories.ContainsKey(type))
            {
                var specificRepositoryType = _specificRepositories[typeof(TEntity)];
                var specificRepository = Activator.CreateInstance(specificRepositoryType, _context);
                _repositories.Add(type, specificRepository);
            }
            else
            {
                _repositories.Add(type, new Repository<TEntity>(_context));
            }

            return (IRepository<TEntity>)_repositories[typeof(TEntity)];
        }


        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        protected void AddSpecificRepository<TEntity, TRepositoryType>() where TEntity : class
        {
            var type = typeof(TEntity);
            var repositoryType = typeof(TRepositoryType);

            _specificRepositories.Add(type, repositoryType);
        }


        private void Dispose(bool disposing)
        {
            if (!disposing) return;
            if (_context == null) return;
            _context.Dispose();
            _context = null;
        }
    }
}