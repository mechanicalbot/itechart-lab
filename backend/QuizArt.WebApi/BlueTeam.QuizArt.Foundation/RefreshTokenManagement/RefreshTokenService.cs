﻿using System.Threading.Tasks;
using BlueTeam.Common.Annotations;
using BlueTeam.QuizArt.DomainModel;
using BlueTeam.QuizArt.Foundation.Interfaces;
using BlueTeam.QuizArt.Repositories.Interfaces;

namespace BlueTeam.QuizArt.Foundation.RefreshTokenManagement
{
    [UsedImplicitly]
    public class RefreshTokenService : IRefreshTokenService
    {
        private readonly IQuizArtUnitOfWork _uow;


        public RefreshTokenService(IQuizArtUnitOfWork uow)
        {
            _uow = uow;
        }


        public async Task<RefreshToken> FindRefreshTokenAsync(string id)
        {
            var refreshTokenRepository = _uow.RefreshTokens;
            var token = await refreshTokenRepository.GetAsync(id);

            return token;
        }

        public async Task DeleteRefreshTokenAsync(RefreshToken token)
        {
            var refreshTokenRepository = _uow.RefreshTokens;
            refreshTokenRepository.Delete(token);
            await _uow.SaveAsync();
        }

        public async Task AddRefreshTokenAsync(RefreshToken token)
        {
            var refreshTokenRepository = _uow.RefreshTokens;
            refreshTokenRepository.Create(token);
            await _uow.SaveAsync();
        }
    }
}