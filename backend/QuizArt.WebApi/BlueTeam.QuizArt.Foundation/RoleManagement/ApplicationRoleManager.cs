﻿using BlueTeam.Common.Annotations;
using BlueTeam.QuizArt.DomainModel;
using Microsoft.AspNet.Identity;

namespace BlueTeam.QuizArt.Foundation.RoleManagement
{
    [UsedImplicitly]
    public class ApplicationRoleManager : RoleManager<Role, int>
    {
        public ApplicationRoleManager(IRoleStore<Role, int> store) 
            : base(store)
        {

        }
    }
}