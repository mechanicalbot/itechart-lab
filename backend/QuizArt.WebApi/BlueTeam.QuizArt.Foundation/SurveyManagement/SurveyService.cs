﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlueTeam.QuizArt.DomainModel;
using BlueTeam.QuizArt.Foundation.Interfaces;
using BlueTeam.QuizArt.Repositories.Interfaces;
using BlueTeam.Common.Annotations;

namespace BlueTeam.QuizArt.Foundation.SurveyManagement
{
    [UsedImplicitly]
    public class SurveyService : ISurveyService
    {
        private readonly IQuizArtUnitOfWork _uow;


        public SurveyService(IQuizArtUnitOfWork uow)
        {
            _uow = uow;
        }


        public async Task<IReadOnlyCollection<Survey>> GetLatestSurveysAsync(int maxCount)
        {
            if (maxCount < 0)
            {
                throw new ArgumentException(@"Number of surveys must be a non-negative integer.", nameof(maxCount));
            }
            var surveysRepository = _uow.Surveys;
            var latestSurveys = await surveysRepository.GetLatestSurveysAsync(maxCount);

            return latestSurveys;
        }
    }
}