﻿using System.Threading.Tasks;
using BlueTeam.Common.Annotations;
using BlueTeam.QuizArt.DomainModel;
using BlueTeam.QuizArt.Foundation.Interfaces;
using BlueTeam.QuizArt.Repositories.Interfaces;

namespace BlueTeam.QuizArt.Foundation.ClientManagement
{
    [UsedImplicitly]
    public class ClientService : IClientService
    {
        private readonly IQuizArtUnitOfWork _uow;


        public ClientService(IQuizArtUnitOfWork uow)
        {
            _uow = uow;
        }


        public async Task<Client> FindClientAsync(string clientId)
        {
            var clientRepository = _uow.GetRepository<Client>();
            var client = await clientRepository.GetAsync(clientId);

            return client;
        }
    }
}