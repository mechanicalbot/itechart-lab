﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BlueTeam.QuizArt.DomainModel;

namespace BlueTeam.QuizArt.Foundation.Interfaces
{
    public interface ISurveyService
    {
        Task<IReadOnlyCollection<Survey>> GetLatestSurveysAsync(int maxCount);
    }
}