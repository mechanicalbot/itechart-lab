﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using BlueTeam.QuizArt.DomainModel;
using BlueTeam.QuizArt.Foundation.UserManagement;

namespace BlueTeam.QuizArt.Foundation.Interfaces
{
    public interface IUserService
    {
        Task<ClaimsIdentity> GenerateUserIdentityAsync(User user, string authenticationType);

        Task<User> FindAsync(string userName, string password);

        Task<User> FindByUserNameAsync(string userName);

        Task<bool> CheckIfEmailOccupiedAsync(string email);

        Task<UserCreationResult> CreateUserAsync(User user, string password);

        Task<UserCreationResult> CreateUserAsync(User user, string password, IReadOnlyCollection<string> roleNames);

        Task<IReadOnlyCollection<UserWithSurveysCount>> GetUsersWithSurveysCountAsync(int skip, int top);

        Task<int> CountUsersAsync();

        Task<UserWithSurveysCount> FindUserWithSurveysCountByUserNameAsync(string userName);

        Task UpdateAsync(User user, string password = null, IReadOnlyCollection<string> roleNames = null);

        Task DeleteAsync(User user);
    }
}