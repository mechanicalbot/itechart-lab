﻿using System.Threading.Tasks;
using BlueTeam.QuizArt.DomainModel;

namespace BlueTeam.QuizArt.Foundation.Interfaces
{
    public interface IClientService
    {
        Task<Client> FindClientAsync(string clientId);
    }
}