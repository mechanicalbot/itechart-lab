﻿using System.Threading.Tasks;
using BlueTeam.QuizArt.DomainModel;

namespace BlueTeam.QuizArt.Foundation.Interfaces
{
    public interface IRefreshTokenService
    {
        Task<RefreshToken> FindRefreshTokenAsync(string id);

        Task DeleteRefreshTokenAsync(RefreshToken token);

        Task AddRefreshTokenAsync(RefreshToken token);
    }
}