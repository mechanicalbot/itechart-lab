﻿using System;
using System.Collections.Generic;

namespace BlueTeam.QuizArt.Foundation.UserManagement.Exceptions
{
    public sealed class UserManagerCreateOperationFailedException : Exception
    {
        public UserManagerCreateOperationFailedException(IEnumerable<string> errors)
        {
            var key = 0;
            foreach (var error in errors)
            {
                Data.Add(key++, error);
            }
        }
    }
}