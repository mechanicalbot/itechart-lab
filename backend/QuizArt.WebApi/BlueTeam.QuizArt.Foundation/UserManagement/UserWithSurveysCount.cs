﻿using BlueTeam.QuizArt.DomainModel;

namespace BlueTeam.QuizArt.Foundation.UserManagement
{
    public class UserWithSurveysCount
    {
        public User User { get; set; }

        public int SurveysCount { get; set; }
    }
}
