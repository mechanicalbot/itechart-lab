﻿using System;
using BlueTeam.QuizArt.DomainModel;

namespace BlueTeam.QuizArt.Foundation.UserManagement
{
    public class UserCreationResult
    {
        public UserCreationStatus Status { get; }

        public User CreatedUser { get; }


        private UserCreationResult(UserCreationStatus status, User user)
        {
            Status = status;
            CreatedUser = user;
        }


        public static UserCreationResult CreateSuccessful(User user)
        {
            var result = new UserCreationResult(UserCreationStatus.Success, user);

            return result;
        }

        public static UserCreationResult CreateUnsuccessful(UserCreationStatus errorStatus)
        {
            if (errorStatus == UserCreationStatus.Success)
            {
                throw new ArgumentException($"Unsuccessful status of user creation couldn't be a {nameof(UserCreationStatus.Success)}");
            }
            var result = new UserCreationResult(errorStatus, null);

            return result;
        }
    }
}