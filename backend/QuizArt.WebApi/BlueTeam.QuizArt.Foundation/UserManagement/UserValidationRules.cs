﻿namespace BlueTeam.QuizArt.Foundation.UserManagement
{
    public static class UserValidationRules
    {
        public const int PasswordMinLength = 6;
        public const int NameMinLength = 2;
    }
}