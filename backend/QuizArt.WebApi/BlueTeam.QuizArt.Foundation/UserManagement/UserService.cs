﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BlueTeam.Common.Annotations;
using BlueTeam.Common.SystemClock;
using BlueTeam.Logging;
using BlueTeam.QuizArt.DomainModel;
using BlueTeam.QuizArt.Foundation.Interfaces;
using BlueTeam.QuizArt.Foundation.UserManagement.Exceptions;
using BlueTeam.QuizArt.Repositories.Interfaces;

namespace BlueTeam.QuizArt.Foundation.UserManagement
{
    [UsedImplicitly]
    public class UserService : IUserService
    {
        private readonly ApplicationUserManager _userManager;
        private readonly ISystemClock _systemClock;
        private readonly ILogger<UserService> _logger;
        private readonly IQuizArtUnitOfWork _uow;


        public UserService(ApplicationUserManager manager, ISystemClock systemClock, ILogger<UserService> logger, IQuizArtUnitOfWork uow)
        {
            _userManager = manager;
            _systemClock = systemClock;
            _logger = logger;
            _uow = uow;
        }


        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(User user, string authenticationType)
        {
            var userIdentity = await _userManager.CreateIdentityAsync(user, authenticationType);

            return userIdentity;
        }

        public async Task<User> FindAsync(string userName, string password)
        {
            var user = await _userManager.FindAsync(userName, password);

            return user;
        }

        public async Task<User> FindByUserNameAsync(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);

            return user;
        }

        public async Task<bool> CheckIfEmailOccupiedAsync(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);

            return user != null;
        }

        public async Task<UserCreationResult> CreateUserAsync(User user, string password)
        {
            return await CreateUserAsync(user, password, new List<string> { RoleNames.User });
        }

        public async Task<UserCreationResult> CreateUserAsync(User user, string password, IReadOnlyCollection<string> roleNames)
        {
            var isEmailOccupied = await CheckIfEmailOccupiedAsync(user.Email);
            if (isEmailOccupied)
            {
                return UserCreationResult.CreateUnsuccessful(UserCreationStatus.EmailOccupied);
            }

            user.JoinDateUtc = _systemClock.UtcNow;
            user.UserName = user.Email;
            var result = await _userManager.CreateAsync(user, password);
            if (!result.Succeeded)
            {
                _logger.Error("An error occurred while creating user", user);
                throw new UserManagerCreateOperationFailedException(result.Errors);
            }
            await AddToRolesAsync(user, roleNames);

            _logger.Info($"User {user.Email} was created.", user);

            return UserCreationResult.CreateSuccessful(user);
        }

        public async Task<IReadOnlyCollection<UserWithSurveysCount>> GetUsersWithSurveysCountAsync(int skip, int top)
        {
            if (skip < 0)
            {
                throw new ArgumentException(@"Skip must be a non-negative integer.", nameof(skip));
            }
            if (top < 0)
            {
                throw new ArgumentException(@"Top must be a non-negative integer.", nameof(top));
            }

            var usersWithSurveysCountDto = await _uow.Users.GetUsersWithSurveysCountAsync(skip, top);
            var usersWithSurveysCount = usersWithSurveysCountDto.Select(CreateFrom);

            return usersWithSurveysCount.ToList();
        }

        public async Task<int> CountUsersAsync()
        {
            var count = await _uow.Users.CountAsync();

            return count;
        }

        public async Task<UserWithSurveysCount> FindUserWithSurveysCountByUserNameAsync(string userName)
        {
            var user = await _uow.Users.FindUserWithSurveysCountByUserNameAsync(userName);

            return CreateFrom(user);
        }

        public async Task UpdateAsync(User user, string password = null, IReadOnlyCollection<string> roleNames = null)
        {
            if (password != null)
            {
                var userPasswordHash = _userManager.PasswordHasher.HashPassword(password);
                user.PasswordHash = userPasswordHash;
            }
            if (roleNames != null)
            {
                await AddToRolesAsync(user, roleNames);

                var actualRoleNames = user.Roles.Select(r => r.Name);
                var roleNamesToRemove = actualRoleNames.Except(roleNames, StringComparer.InvariantCultureIgnoreCase);
                await RemoveFromRolesAsync(user, roleNamesToRemove);
            }

            await _userManager.UpdateAsync(user);
        }

        public async Task DeleteAsync(User user)
        {
            await _userManager.DeleteAsync(user);
        }
        

        private async Task AddToRolesAsync(User user, IEnumerable<string> roleNames)
        {
            await _userManager.AddToRolesAsync(user.Id, roleNames.ToArray());
        }

        private async Task RemoveFromRolesAsync(User user, IEnumerable<string> roleNames)
        {
            await _userManager.RemoveFromRolesAsync(user.Id, roleNames.ToArray());
        }

        private static UserWithSurveysCount CreateFrom(Repositories.Dto.UserWithSurveysCount userWithSurveysCount)
        {
            return new UserWithSurveysCount
            {
                User = userWithSurveysCount.User,
                SurveysCount = userWithSurveysCount.SurveysCount
            };
        }
    }
}