﻿using System.Security.Claims;
using System.Threading.Tasks;
using BlueTeam.Common.Annotations;
using BlueTeam.QuizArt.DomainModel;
using Microsoft.AspNet.Identity;

namespace BlueTeam.QuizArt.Foundation.UserManagement
{
    [UsedImplicitly]
    public class ApplicationUserManager : UserManager<User, int>
    {
        private const string NameClaim = "name";


        public ApplicationUserManager(IUserStore<User, int> store)
            : base(store)
        {
            UserLockoutEnabledByDefault = false;
            UserValidator = new UserValidator<User, int>(this)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            PasswordValidator = new PasswordValidator
            {
                RequiredLength = UserValidationRules.PasswordMinLength,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false
            };
        }


        public override async Task<ClaimsIdentity> CreateIdentityAsync(User user, string authenticationType)
        {
            var baseIdentity = await base.CreateIdentityAsync(user, authenticationType);
            baseIdentity.AddClaim(new Claim(NameClaim, user.DisplayName));

            return baseIdentity;
        }
    }
}