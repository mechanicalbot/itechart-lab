﻿namespace BlueTeam.QuizArt.Foundation.UserManagement
{
    public enum UserCreationStatus
    {
        Success,
        EmailOccupied
    }
}