﻿using System.Collections.Generic;
using Microsoft.AspNet.Identity;

namespace BlueTeam.QuizArt.DomainModel
{
    public class Role : IRole<int>
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<User> Users { get; set; }
    }
}