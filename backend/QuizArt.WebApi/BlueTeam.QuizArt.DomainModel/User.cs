﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;

namespace BlueTeam.QuizArt.DomainModel
{
    public class User : IUser<int>
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string DisplayName { get; set; }

        public string PasswordHash { get; set; }

        public DateTime JoinDateUtc { get; set; }

        public ICollection<Role> Roles { get; set; }

        public ICollection<Survey> Surveys { get; set; }
    }
}