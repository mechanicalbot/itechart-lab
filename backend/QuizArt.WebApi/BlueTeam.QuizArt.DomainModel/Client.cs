﻿using System;

namespace BlueTeam.QuizArt.DomainModel
{
    public class Client
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public bool IsActive { get; set; }

        public string Secret { get; set; }

        public string AllowedOrigin { get; set; }

        public long RefreshTokenLifeTimeMilliseconds { get; set; }

        public TimeSpan RefreshTokenLifeTime
        {
            get => TimeSpan.FromMilliseconds(RefreshTokenLifeTimeMilliseconds);
            set => RefreshTokenLifeTimeMilliseconds = Convert.ToInt64(value.TotalMilliseconds);
        }
    }
}