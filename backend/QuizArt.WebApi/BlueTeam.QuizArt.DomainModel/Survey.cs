﻿using System;

namespace BlueTeam.QuizArt.DomainModel
{
    public class Survey
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime CreationDate { get; set; }

        public User Author { get; set; }

        public int AuthorId { get; set; }
    }
}