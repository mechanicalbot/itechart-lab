﻿namespace BlueTeam.QuizArt.DomainModel
{
    public static class RoleNames
    {
        public const string User = "User";
        public const string Admin = "Admin";
    }
}