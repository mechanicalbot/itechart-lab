﻿using System;

namespace BlueTeam.QuizArt.DomainModel
{
    public class RefreshToken
    {
        public string Id { get; set; }

        public User User { get; set; }

        public int UserId { get; set; }

        public DateTime IssuedDateUtc { get; set; }

        public DateTime ExpiresDateUtc { get; set; }

        public string ProtectedTicket { get; set; }

        public Client Client { get; set; }

        public string ClientId { get; set; }
    }
}