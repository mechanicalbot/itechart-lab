﻿using System;
using System.Web.Http;
using Microsoft.Web.Http;
using Microsoft.Web.Http.Versioning.Conventions;

namespace BlueTeam.Web.Api.Common.Versioning
{
    public static class ApiVersioningExtensions
    {
        public static void AddNamespaceBasedApiVersioning(this HttpConfiguration config)
        {
            config.AddApiVersioning(options =>
            {
                var assembliesResolver = config.Services.GetAssembliesResolver();
                var controllerTypeResolver = config.Services.GetHttpControllerTypeResolver();
                var controllerTypes = controllerTypeResolver.GetControllerTypes(assembliesResolver);
                var getControllerConventionBuilderGenericMethod = typeof(ApiVersionConventionBuilder).GetMethod(nameof(ApiVersionConventionBuilder.Controller));
                foreach (var controllerType in controllerTypes)
                {
                    var controllerApiVersion = GetApiVersionFromNamespace(controllerType);
                    var getControllerConventionBuilderMethod = getControllerConventionBuilderGenericMethod.MakeGenericMethod(controllerType);
                    var controllerConventionBuilder = getControllerConventionBuilderMethod.Invoke(options.Conventions, null);
                    var specifyApiVersionMethod = controllerConventionBuilder.GetType().GetMethod(nameof(ControllerApiVersionConventionBuilder<ApiController>.HasApiVersion));
                    specifyApiVersionMethod.Invoke(controllerConventionBuilder, new object[] { controllerApiVersion });
                }
            });
        }


        private static ApiVersion GetApiVersionFromNamespace(Type controllerType)
        {
            // ReSharper disable once PossibleNullReferenceException
            var segments = controllerType.Namespace.Split(Type.Delimiter);
            var version = segments[segments.Length - 2];
            var versionNumber = Int32.Parse(version.Remove(0, 1));
            var apiVersion = new ApiVersion(versionNumber, 0);

            return apiVersion;
        }
    }
}