﻿using System.Net;
using System.Web.Http;
using System.Web.Http.Results;
using BlueTeam.Web.Api.Common.DataContracts;

namespace BlueTeam.Web.Api.Common.Controllers
{
    public abstract class BaseApiController : ApiController
    {
        protected NegotiatedContentResult<object> ValidationError(string message)
        {
            var content = new NegotiatedContentResult<object>(HttpStatusCode.BadRequest, new ErrorDataContract(message), this);

            return content;
        }
    }
}