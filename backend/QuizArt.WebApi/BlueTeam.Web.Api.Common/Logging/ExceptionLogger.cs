﻿using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.ExceptionHandling;
using BlueTeam.Logging;

namespace BlueTeam.Web.Api.Common.Logging
{
    public class ExceptionLogger : System.Web.Http.ExceptionHandling.ExceptionLogger
    {
        private readonly ILogger _logger;


        public ExceptionLogger(ILogger logger)
        {
            _logger = logger;
        }


        public override async Task LogAsync(ExceptionLoggerContext context, CancellationToken cancellationToken)
        {
            var request = context.Request;
            var content = await request.Content.ReadAsStringAsync();
            var uri = request.RequestUri;
            var method = request.Method;
            _logger.Error(context.Exception, "An unhandled exception occurred. Request: {0} {1} Content: {{{2}}}", method, uri, content);
            await base.LogAsync(context, cancellationToken);
        }
    }
}