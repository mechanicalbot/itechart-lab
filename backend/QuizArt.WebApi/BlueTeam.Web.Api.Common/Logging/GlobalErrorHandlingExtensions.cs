﻿using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using BlueTeam.Logging;

namespace BlueTeam.Web.Api.Common.Logging
{
    public static class GlobalErrorHandlingExtensions
    {
        public static void AddGlobalErrorLogging(this HttpConfiguration config)
        {
            var loggerFactory = (ILoggerFactory)config.DependencyResolver.GetService(typeof(ILoggerFactory));
            var logger = loggerFactory.Create(typeof(ExceptionLogger).FullName);
            config.Services.Add(typeof(IExceptionLogger), new ExceptionLogger(logger));
        }
    }
}