﻿using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace BlueTeam.Web.Api.Common.DataContracts
{
    public class ModelStateErrorDataContract : ErrorDataContract
    {
        public HttpError ModelState { get; }


        public ModelStateErrorDataContract(string message, ModelStateDictionary modelState) 
            : base(message)
        {
            ModelState = new HttpError(modelState, true).ModelState;
        }
    }
}