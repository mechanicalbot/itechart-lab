﻿namespace BlueTeam.Web.Api.Common.DataContracts
{
    public class ErrorDataContract
    {
        public string Error { get; }


        public ErrorDataContract(string message)
        {
            Error = message;
        }
    }
}