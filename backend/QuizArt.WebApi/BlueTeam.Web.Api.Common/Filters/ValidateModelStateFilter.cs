﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using BlueTeam.Web.Api.Common.Constants;
using BlueTeam.Web.Api.Common.DataContracts;

namespace BlueTeam.Web.Api.Common.Filters
{
    public class ValidateModelStateFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!actionContext.ModelState.IsValid)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest,
                    new ModelStateErrorDataContract(ValidationConstants.InvalidInput, actionContext.ModelState));
            }
        }
    }
}