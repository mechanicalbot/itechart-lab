﻿using BlueTeam.Common.Annotations;
using NLog;

namespace BlueTeam.Logging.NLog
{
    [UsedImplicitly]
    public class NLogLoggerFactory : ILoggerFactory
    {
        public ILogger Create(string name)
        {
            var logger = LogManager.GetLogger(name);

            return new NLogLogger(logger);
        }
    }
}