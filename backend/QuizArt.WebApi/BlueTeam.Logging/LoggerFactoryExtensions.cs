﻿using System;

namespace BlueTeam.Logging
{
    public static class LoggerFactoryExtensions
    {
        public static ILogger Create<T>(this ILoggerFactory loggerFactory)
        {
            return loggerFactory.Create(typeof(T).FullName);
        }

        public static ILogger Create(this ILoggerFactory loggerFactory, Type type)
        {
            return loggerFactory.Create(type.FullName);
        }
    }
}
