﻿namespace BlueTeam.Logging
{
    public interface ILoggerFactory
    {
        ILogger Create(string name);
    }
}