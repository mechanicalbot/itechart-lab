﻿using System;

namespace BlueTeam.Logging
{
    public class Logger<T> : ILogger<T>
    {
        private readonly ILogger _logger;


        public Logger(ILoggerFactory factory)
        {
            _logger = factory.Create(typeof(T));
        }


        public void Trace(string format, params object[] args)
        {
            _logger.Trace(format, args);
        }

        public void Trace(Exception exception, string format, params object[] args)
        {
            _logger.Trace(exception, format, args);
        }

        public void Debug(string format, params object[] args)
        {
            _logger.Debug(format, args);
        }

        public void Debug(Exception exception, string format, params object[] args)
        {
            _logger.Debug(exception, format, args);
        }

        public void Info(string format, params object[] args)
        {
            _logger.Info(format, args);
        }

        public void Info(Exception exception, string format, params object[] args)
        {
            _logger.Info(exception, format, args);
        }

        public void Warn(string format, params object[] args)
        {
            _logger.Warn(format, args);
        }

        public void Warn(Exception exception, string format, params object[] args)
        {
            _logger.Warn(exception, format, args);
        }

        public void Error(string format, params object[] args)
        {
            _logger.Error(format, args);
        }

        public void Error(Exception exception, string format, params object[] args)
        {
            _logger.Error(exception, format, args);
        }

        public void Fatal(string format, params object[] args)
        {
            _logger.Fatal(format, args);
        }

        public void Fatal(Exception exception, string format, params object[] args)
        {
            _logger.Fatal(exception, format, args);
        }
    }
}