﻿using System;

namespace BlueTeam.Logging
{
    public interface ILogger
    {
        void Trace(string format, params object[] args);

        void Trace(Exception exception, string format, params object[] args);

        void Debug(string format, params object[] args);

        void Debug(Exception exception, string format, params object[] args);

        void Info(string format, params object[] args);

        void Info(Exception exception, string format, params object[] args);

        void Warn(string format, params object[] args);

        void Warn(Exception exception, string format, params object[] args);

        void Error(string format, params object[] args);

        void Error(Exception exception, string format, params object[] args);

        void Fatal(string format, params object[] args);

        void Fatal(Exception exception, string format, params object[] args);
    }
}