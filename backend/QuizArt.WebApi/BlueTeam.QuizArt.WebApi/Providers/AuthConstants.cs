﻿namespace BlueTeam.QuizArt.WebApi.Providers
{
    internal static class AuthConstants
    {
        public static class ContextKeys
        {
            public const string ClientAllowedOrigin = "clientAllowedOrigin";
            public const string ClientId = "clientId";
        }

        public static class AuthErrorCodes
        {
            public const string InvalidClient = "invalid_client";
            public const string InvalidGrant = "invalid_grant";
        }
    }
}