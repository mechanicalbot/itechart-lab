﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Cors;
using BlueTeam.Common.Extensions;
using BlueTeam.QuizArt.Foundation.Interfaces;
using BlueTeam.Web.Api.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;

namespace BlueTeam.QuizArt.WebApi.Providers
{
    public class CustomOAuthProvider : OAuthAuthorizationServerProvider
    {
        private const string AuthenticationType = "JWT";
        private const string AuthorizationHeader = "authorization";


        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var serviceProvider = context.OwinContext.Get<IServiceProvider>();
            var userService = serviceProvider.GetService<IUserService>();

            var user = await userService.FindAsync(context.UserName, context.Password);
            if (user == null)
            {
                context.SetError(AuthConstants.AuthErrorCodes.InvalidGrant, "The user name or password is incorrect.");
                return;
            }

            var oAuthIdentity = await userService.GenerateUserIdentityAsync(user, AuthenticationType);
            var props = new AuthenticationProperties();
            var ticket = new AuthenticationTicket(oAuthIdentity, props);
            context.Validated(ticket);
        }

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            var serviceProvider = context.OwinContext.Get<IServiceProvider>();
            var clientService = serviceProvider.GetService<IClientService>();

            if (!context.TryGetBasicCredentials(out string clientId, out string clientSecret) && !context.TryGetFormCredentials(out clientId, out clientSecret))
            {
                context.SetError(AuthConstants.AuthErrorCodes.InvalidClient, "ClientId must be sent.");
                return;
            }
            if (context.ClientId == null)
            {
                context.SetError(AuthConstants.AuthErrorCodes.InvalidClient, "ClientId must be sent.");
                return;
            }

            var client = await clientService.FindClientAsync(context.ClientId);
            if (client == null)
            {
                context.SetError(AuthConstants.AuthErrorCodes.InvalidClient, $"Client '{context.ClientId}' is not registered in the system.");
                return;
            }
            if (!client.IsActive)
            {
                context.SetError(AuthConstants.AuthErrorCodes.InvalidClient, "Client is inactive.");
                return;
            }

            if (client.Secret != null)
            {
                if (string.IsNullOrEmpty(clientSecret))
                {
                    context.SetError(AuthConstants.AuthErrorCodes.InvalidGrant, "Client secret must be sent.");
                    return;
                }
                if (clientSecret != client.Secret)
                {
                    context.SetError(AuthConstants.AuthErrorCodes.InvalidGrant, "Client secret is incorrect.");
                    return;
                }
            }

            context.Response.Headers.Set(CorsConstants.AccessControlAllowOrigin, client.AllowedOrigin);
            context.OwinContext.Set(AuthConstants.ContextKeys.ClientId, clientId);
            context.Validated();
        }

        public override Task MatchEndpoint(OAuthMatchEndpointContext context)
        {
            if (!context.IsTokenEndpoint || context.Request.Method != HttpMethod.Options.Method)
            {
                return base.MatchEndpoint(context);
            }
            context.OwinContext.Response.Headers.Add(CorsConstants.AccessControlAllowOrigin, new[] { CorsConstants.AnyOrigin });
            context.OwinContext.Response.Headers.Add(CorsConstants.AccessControlAllowHeaders, new[] { AuthorizationHeader });
            context.RequestCompleted();

            return Task.FromResult(0);
        }
    }
}