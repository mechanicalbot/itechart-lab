﻿using System;
using System.Threading.Tasks;
using System.Web.Cors;
using BlueTeam.Common.Extensions;
using BlueTeam.Common.HashCodeCalculation;
using BlueTeam.Common.SystemClock;
using BlueTeam.QuizArt.DomainModel;
using BlueTeam.QuizArt.Foundation.Interfaces;
using BlueTeam.Web.Api.Owin;
using Microsoft.Owin.Security.Infrastructure;

namespace BlueTeam.QuizArt.WebApi.Providers
{
    public class RefreshTokenProvider : IAuthenticationTokenProvider
    {
        private readonly IHashCodeCalculator _hashCalculator;
        private readonly ISystemClock _systemClock;


        public RefreshTokenProvider(IHashCodeCalculator hashCalculator, ISystemClock systemClock)
        {
            _hashCalculator = hashCalculator;
            _systemClock = systemClock;
        }


        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            var clientId = context.OwinContext.Get<string>(AuthConstants.ContextKeys.ClientId);

            var serviceProvider = context.OwinContext.Get<IServiceProvider>();
            var clientService = serviceProvider.GetService<IClientService>();
            var userService = serviceProvider.GetService<IUserService>();
            var refreshTokenService = serviceProvider.GetService<IRefreshTokenService>();

            if (string.IsNullOrEmpty(clientId))
            {
                return;
            }

            var client = await clientService.FindClientAsync(clientId);
            var user = await userService.FindByUserNameAsync(context.Ticket.Identity.Name);
            var refreshTokenId = Guid.NewGuid().ToString("n");
            var refreshTokenLifeTime = client.RefreshTokenLifeTime;
            var token = new RefreshToken
            {
                Id = _hashCalculator.GetHash(refreshTokenId),
                Client = client,
                ClientId = client.Id,
                User = user,
                UserId = user.Id,
                IssuedDateUtc = _systemClock.UtcNow,
                ExpiresDateUtc = _systemClock.UtcNow.Add(refreshTokenLifeTime)
            };
            context.Ticket.Properties.IssuedUtc = token.IssuedDateUtc;
            context.Ticket.Properties.ExpiresUtc = token.ExpiresDateUtc;
            token.ProtectedTicket = context.SerializeTicket();
            await refreshTokenService.AddRefreshTokenAsync(token);
            context.SetToken(refreshTokenId);
        }

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            var serviceProvider = context.OwinContext.Get<IServiceProvider>();
            var refreshTokenService = serviceProvider.GetService<IRefreshTokenService>();

            var hashedTokenId = _hashCalculator.GetHash(context.Token);
            var refreshToken = await refreshTokenService.FindRefreshTokenAsync(hashedTokenId);
            if (refreshToken != null)
            {
                context.DeserializeTicket(refreshToken.ProtectedTicket);
                await refreshTokenService.DeleteRefreshTokenAsync(refreshToken);
            }
        }

        public void Create(AuthenticationTokenCreateContext context)
        {
            throw new NotImplementedException();
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            throw new NotImplementedException();
        }
    }
}