﻿using System;
using System.Runtime.Serialization;

namespace BlueTeam.QuizArt.WebApi.V2.DataContracts
{
    [DataContract]
    internal class SurveyDataContract
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public DateTime CreationDate { get; set; }


        public SurveyDataContract(int id, string name, DateTime date)
        {
            Id = id;
            Name = name;
            CreationDate = date;
        }
    }
}