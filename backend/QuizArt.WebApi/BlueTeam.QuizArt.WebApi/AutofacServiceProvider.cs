﻿using System;
using Autofac;
using Autofac.Integration.Owin;
using Microsoft.Owin;

namespace BlueTeam.QuizArt.WebApi
{
    public class AutofacServiceProvider : IServiceProvider
    {
        private readonly IOwinContext _context;


        public AutofacServiceProvider(IOwinContext context)
        {
            _context = context;
        }


        public object GetService(Type serviceType)
        {
            var autofacLifeTimeScope = _context.GetAutofacLifetimeScope();
            var service = autofacLifeTimeScope.Resolve(serviceType);

            return service;
        }
    }
}