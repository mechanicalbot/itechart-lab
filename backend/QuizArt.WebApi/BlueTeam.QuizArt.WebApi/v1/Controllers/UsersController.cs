﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using BlueTeam.QuizArt.DomainModel;
using BlueTeam.QuizArt.Foundation.Interfaces;
using BlueTeam.QuizArt.Foundation.UserManagement;
using BlueTeam.QuizArt.WebApi.V1.DataContracts;
using BlueTeam.QuizArt.WebApi.V1.DataContracts.Users;
using BlueTeam.QuizArt.WebApi.V1.Validation;
using BlueTeam.Web.Api.Common.Controllers;

namespace BlueTeam.QuizArt.WebApi.V1.Controllers
{
    [Authorize(Roles = RoleNames.Admin)]
    public class UsersController : BaseApiController
    {
        private readonly IUserService _userService;


        public UsersController(IUserService userService)
        {
            _userService = userService;
        }


        public async Task<IHttpActionResult> GetAsync(int skip = 0, int top = 5)
        {
            if (skip < 0)
            {
                return BadRequest("Skip must be a non-negative integer.");
            }
            if (top < 0)
            {
                return BadRequest("Top must be a non-negative integer.");
            }

            var usersWithSurveysCounts = await _userService.GetUsersWithSurveysCountAsync(skip, top);
            var userDataContracts = usersWithSurveysCounts.Select(CreateFrom).ToList();
            var totalUsersCount = await _userService.CountUsersAsync();
            var paginatedDataContract = new DataContractWithTotalItemsCount<UserWithSurveysCountDataContract>(totalUsersCount, userDataContracts);

            return Ok(paginatedDataContract);
        }

        public async Task<IHttpActionResult> GetAsync(string userName)
        {
            if (userName == null)
            {
                return BadRequest("UserName must not be null.");
            }

            var userWithSurveysCount = await _userService.FindUserWithSurveysCountByUserNameAsync(userName);
            if (userWithSurveysCount == null)
            {
                return NotFound();
            }

            var userDataContract = CreateFrom(userWithSurveysCount);

            return Ok(userDataContract);
        }

        public async Task<IHttpActionResult> PostAsync(CreateUserDataContract createUserDataContract)
        {
            var user = CreateFrom(createUserDataContract);

            var result = await _userService.CreateUserAsync(user, createUserDataContract.Password, createUserDataContract.Roles);
            switch (result.Status)
            {
                case UserCreationStatus.EmailOccupied:
                {
                    return ValidationError(ValidationErrorTypes.EmailOccupied);
                }
                case UserCreationStatus.Success:
                {
                    var currentUri = Url.Request.RequestUri;
                    var createdUserDataContract = CreateFrom(result.CreatedUser);

                    return Created(currentUri, createdUserDataContract);
                }
                default:
                {
                    throw new ArgumentOutOfRangeException(nameof(result.Status), result.Status, @"UserCreationStatus is out of range.");
                }
            }
        }

        public async Task<IHttpActionResult> PutAsync(string userName, UpdateUserDataContract updateUserDataContract)
        {
            var user = await _userService.FindByUserNameAsync(userName);
            if (user == null)
            {
                return NotFound();
            }

            user.DisplayName = updateUserDataContract.DisplayName;
            await _userService.UpdateAsync(user, updateUserDataContract.Password, updateUserDataContract.Roles);

            return Ok();
        }

        public async Task<IHttpActionResult> DeleteAsync(string userName)
        {
            var user = await _userService.FindByUserNameAsync(userName);
            if (user == null)
            {
                return NotFound();
            }

            await _userService.DeleteAsync(user);

            return Ok();
        }


        private static UserWithSurveysCountDataContract CreateFrom(UserWithSurveysCount userWithSurveysCount)
        {
            return new UserWithSurveysCountDataContract(userWithSurveysCount.User.UserName,
                userWithSurveysCount.User.DisplayName,
                userWithSurveysCount.User.JoinDateUtc,
                userWithSurveysCount.SurveysCount,
                userWithSurveysCount.User.Roles.Select(r => r.Name).ToList());
        }

        private static CreatedUserDataContract CreateFrom(User user)
        {
            return new CreatedUserDataContract(user.Email, user.DisplayName, user.JoinDateUtc, user.Roles.Select(u => u.Name).ToList());
        }

        private static User CreateFrom(CreateUserDataContract createUserDataContract)
        {
            return new User
            {
                DisplayName = createUserDataContract.DisplayName,
                UserName = createUserDataContract.Email,
                Email = createUserDataContract.Email
            };
        }
    }
}