﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using BlueTeam.QuizArt.DomainModel;
using BlueTeam.QuizArt.Foundation.Interfaces;
using BlueTeam.QuizArt.WebApi.V1.DataContracts.Surveys;
using BlueTeam.Web.Api.Common.Controllers;

namespace BlueTeam.QuizArt.WebApi.V1.Controllers
{
    public class SurveysController : BaseApiController
    {
        private readonly ISurveyService _surveyService;


        public SurveysController(ISurveyService service)
        {
            _surveyService = service;
        }


        [Authorize(Roles = RoleNames.User)]
        public async Task<IHttpActionResult> GetAsync(int max)
        {
            if (max < 0)
            {
                return BadRequest("Number of surveys must be a non-negative integer.");
            }

            var latestSurveys = await _surveyService.GetLatestSurveysAsync(max);
            var surveysDataContracts = latestSurveys.Select(CreateFrom).ToList();

            return Ok(surveysDataContracts);
        }


        private static SurveyDataContract CreateFrom(Survey survey)
        {
            return new SurveyDataContract(survey.Id, survey.Name, survey.CreationDate);
        }
    }
}