﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using BlueTeam.QuizArt.DomainModel;
using BlueTeam.QuizArt.Foundation.Interfaces;
using BlueTeam.QuizArt.Foundation.UserManagement;
using BlueTeam.QuizArt.WebApi.V1.DataContracts.Accounts;
using BlueTeam.QuizArt.WebApi.V1.Validation;
using BlueTeam.Web.Api.Common.Controllers;

namespace BlueTeam.QuizArt.WebApi.V1.Controllers
{
    public class AccountsController : BaseApiController
    {
        private const string DefaultLocation = "http://localhost:8080/login";


        private readonly IUserService _userService;


        public AccountsController(IUserService service)
        {
            _userService = service;
        }


        [HttpPost]
        public async Task<IHttpActionResult> SignUp(SignUpUserDataContract signUpUserDataContract)
        {
            var newUser = CreateFrom(signUpUserDataContract);
            var result = await _userService.CreateUserAsync(newUser, signUpUserDataContract.Password);
            switch (result.Status)
            {
                case UserCreationStatus.EmailOccupied:
                {
                    return ValidationError(ValidationErrorTypes.EmailOccupied);
                }
                case UserCreationStatus.Success:
                {
                    return Created(new Uri(DefaultLocation), CreateFrom(result.CreatedUser));
                }
                default:
                {
                    throw new ArgumentOutOfRangeException(nameof(result.Status), result.Status, @"UserCreationStatus is out of range.");
                }
            }
        }

        [HttpGet]
        public async Task<IHttpActionResult> CheckEmail(string email)
        {
            var isUserExists = await _userService.CheckIfEmailOccupiedAsync(email);

            return Ok(new IsEmailOccupiedDataContract(isUserExists));
        }


        private static User CreateFrom(SignUpUserDataContract signUpUserDataContract)
        {
            var user = new User
            {
                DisplayName = signUpUserDataContract.DisplayName,
                Email = signUpUserDataContract.Email
            };

            return user;
        }

        private static CreatedUserDataContract CreateFrom(User user)
        {
            var createdUserDataContract = new CreatedUserDataContract(user.Email, user.DisplayName, user.JoinDateUtc);

            return createdUserDataContract;
        }
    }
}