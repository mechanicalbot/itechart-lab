﻿using System;
using System.Collections.Generic;

namespace BlueTeam.QuizArt.WebApi.V1.DataContracts.Users
{
    public class UserWithSurveysCountDataContract
    {
        public string UserName { get; }

        public string DisplayName { get; }

        public DateTime JoinDate { get; }

        public int SurveysCount { get; }

        public IReadOnlyCollection<string> Roles { get; }


        public UserWithSurveysCountDataContract(string userName, string displayName, DateTime joinDate, int surveysCount, IReadOnlyCollection<string> roles)
        {
            UserName = userName;
            DisplayName = displayName;
            JoinDate = joinDate;
            SurveysCount = surveysCount;
            Roles = roles;
        }
    }
}