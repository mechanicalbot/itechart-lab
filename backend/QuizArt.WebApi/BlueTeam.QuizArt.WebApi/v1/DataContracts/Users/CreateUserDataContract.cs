﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BlueTeam.QuizArt.Foundation.UserManagement;

namespace BlueTeam.QuizArt.WebApi.V1.DataContracts.Users
{
    public class CreateUserDataContract
    {
        [Required]
        [MinLength(UserValidationRules.NameMinLength)]
        public string DisplayName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [MinLength(UserValidationRules.PasswordMinLength)]
        public string Password { get; set; }

        [Required]
        public IReadOnlyCollection<string> Roles { get; set; }
    }
}