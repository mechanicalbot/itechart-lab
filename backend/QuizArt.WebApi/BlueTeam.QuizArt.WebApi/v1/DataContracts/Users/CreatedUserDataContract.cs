﻿using System;
using System.Collections.Generic;

namespace BlueTeam.QuizArt.WebApi.V1.DataContracts.Users
{
    public class CreatedUserDataContract
    {
        public string UserName { get; }

        public string DisplayName { get; }

        public DateTime JoinDateUtc { get; }

        public IReadOnlyCollection<string> Roles { get; }

        public CreatedUserDataContract(string userName, string displayName, DateTime joinDateUtc, IReadOnlyCollection<string> roles)
        {
            UserName = userName;
            DisplayName = displayName;
            JoinDateUtc = joinDateUtc;
            Roles = roles;
        }
    }
}