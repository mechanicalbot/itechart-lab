﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BlueTeam.QuizArt.Foundation.UserManagement;

namespace BlueTeam.QuizArt.WebApi.V1.DataContracts.Users
{
    public class UpdateUserDataContract
    {
        [MinLength(UserValidationRules.NameMinLength)]
        [Required]
        public string DisplayName { get; set; }
        
        [MinLength(UserValidationRules.PasswordMinLength)]
        public string Password { get; set; }

        [Required]
        public IReadOnlyCollection<string> Roles { get; set; }
    }
}