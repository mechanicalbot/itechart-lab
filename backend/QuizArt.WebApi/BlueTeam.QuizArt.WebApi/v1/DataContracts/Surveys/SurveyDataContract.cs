﻿using System;

namespace BlueTeam.QuizArt.WebApi.V1.DataContracts.Surveys
{
    public class SurveyDataContract
    {
        public int Id { get; }

        public string Name { get; }

        public DateTime CreationDate { get; }


        public SurveyDataContract(int id, string name, DateTime date)
        {
            Id = id;
            Name = name;
            CreationDate = date;
        }
    }
}