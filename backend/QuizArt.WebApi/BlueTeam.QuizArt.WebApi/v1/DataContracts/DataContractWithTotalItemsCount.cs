﻿using System.Collections.Generic;

namespace BlueTeam.QuizArt.WebApi.V1.DataContracts
{
    public class DataContractWithTotalItemsCount<T>
    {
        public int TotalItemsCount { get; }

        public IReadOnlyCollection<T> Data { get; }


        public DataContractWithTotalItemsCount(int totalItemsCount, IReadOnlyCollection<T> data)
        {
            TotalItemsCount = totalItemsCount;
            Data = data;
        }
    }
}