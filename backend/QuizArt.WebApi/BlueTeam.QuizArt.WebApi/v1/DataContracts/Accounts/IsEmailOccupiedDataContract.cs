﻿namespace BlueTeam.QuizArt.WebApi.V1.DataContracts.Accounts
{
    public class IsEmailOccupiedDataContract
    {
        public bool IsEmailOccupied { get; }


        public IsEmailOccupiedDataContract(bool isOccupied)
        {
            IsEmailOccupied = isOccupied;
        }
    }
}