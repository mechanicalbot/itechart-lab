﻿using System;

namespace BlueTeam.QuizArt.WebApi.V1.DataContracts.Accounts
{
    public class CreatedUserDataContract
    {
        public string UserName { get; }

        public string DisplayName { get; }

        public DateTime JoinDateUtc { get; }


        public CreatedUserDataContract(string userName, string displayName, DateTime joinDateUtc)
        {
            UserName = userName;
            DisplayName = displayName;
            JoinDateUtc = joinDateUtc;
        }
    }
}