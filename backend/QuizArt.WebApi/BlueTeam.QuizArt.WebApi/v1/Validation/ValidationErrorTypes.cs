﻿namespace BlueTeam.QuizArt.WebApi.V1.Validation
{
    public static class ValidationErrorTypes
    {
        public const string EmailOccupied = "email_occupied";
    }
}