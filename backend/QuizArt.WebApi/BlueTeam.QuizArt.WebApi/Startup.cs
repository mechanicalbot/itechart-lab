﻿using System;
using System.Web.Http;
using Autofac.Integration.WebApi;
using BlueTeam.Common.Annotations;
using BlueTeam.Web.Api.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;

[assembly: OwinStartup(typeof(BlueTeam.QuizArt.WebApi.Startup))]

namespace BlueTeam.QuizArt.WebApi
{
    [UsedImplicitly]
    public class Startup
    {
        [UsedImplicitly]
        public void Configuration(IAppBuilder app)
        {
            var httpConfig = new HttpConfiguration();

            var container = AutofacConfig.Create();
            httpConfig.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            app.UseAutofacMiddleware(container);

            WebApiConfig.Register(httpConfig);
            RouteConfig.Register(httpConfig);

            app.CreateInstancePerOwinContext<IServiceProvider>(ctx => new AutofacServiceProvider(ctx));

            app.UseOAuthAuthorizationServer(AuthConfig.GetOAuthAuthorizationServerOptions(httpConfig));
            app.UseJwtBearerAuthentication(AuthConfig.GetJwtBearerAuthenticationOptions());

            app.UseCors(CorsOptions.AllowAll);

            app.UseAutofacWebApi(httpConfig);
            app.UseWebApi(httpConfig);
        }
    }
}