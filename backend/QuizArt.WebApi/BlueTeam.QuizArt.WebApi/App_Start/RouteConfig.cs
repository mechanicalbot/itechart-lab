﻿using System.Web.Http;
using Microsoft.Web.Http.Routing;

namespace BlueTeam.QuizArt.WebApi
{
    public static class RouteConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "VersionedApiRouteWithUserName",
                routeTemplate: "api/v{apiVersion}/users/{userName}",
                defaults: new { controller = "Users", userName = RouteParameter.Optional },
                constraints: new { apiVersion = new ApiVersionRouteConstraint() });
            config.Routes.MapHttpRoute(
                name: "VersionedApiRoute",
                routeTemplate: "api/v{apiVersion}/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional },
                constraints: new { apiVersion = new ApiVersionRouteConstraint() });
        }
    }
}