﻿using System.Data.Entity;
using System.Reflection;
using Autofac;
using Autofac.Integration.WebApi;
using BlueTeam.Common.HashCodeCalculation;
using BlueTeam.Common.SystemClock;
using BlueTeam.Logging.NLog;
using BlueTeam.QuizArt.Foundation.Interfaces;
using BlueTeam.QuizArt.Foundation.SurveyManagement;
using BlueTeam.QuizArt.Repositories;
using BlueTeam.QuizArt.Repositories.Interfaces;
using BlueTeam.Logging;
using BlueTeam.QuizArt.DomainModel;
using BlueTeam.QuizArt.Foundation.ClientManagement;
using BlueTeam.QuizArt.Foundation.RefreshTokenManagement;
using BlueTeam.QuizArt.Foundation.RoleManagement;
using BlueTeam.QuizArt.Foundation.UserManagement;
using BlueTeam.QuizArt.Repositories.AspNetIdentity;
using Microsoft.AspNet.Identity;

namespace BlueTeam.QuizArt.WebApi
{
    public static class AutofacConfig
    {
        public static IContainer Create()
        {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            RegisterTypes(builder);
            var container = builder.Build();

            return container;
        }


        private static void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterType<QuizArtDbContext>().As<DbContext>().InstancePerRequest();
            builder.RegisterType<QuizArtUnitOfWork>().As<IQuizArtUnitOfWork>().InstancePerRequest();

            builder.RegisterType<SurveyService>().As<ISurveyService>().InstancePerRequest();
            builder.RegisterType<UserService>().As<IUserService>().InstancePerRequest();
            builder.RegisterType<RefreshTokenService>().As<IRefreshTokenService>().InstancePerRequest();
            builder.RegisterType<ClientService>().As<IClientService>().InstancePerRequest();

            builder.RegisterType<ApplicationUserManager>().InstancePerRequest();
            builder.RegisterType<ApplicationRoleManager>().InstancePerRequest();

            builder.RegisterType<UserStore>().As<IUserStore<User, int>>().InstancePerRequest();
            builder.RegisterType<RoleStore>().As<IRoleStore<Role, int>>().InstancePerRequest();

            builder.RegisterType<NLogLoggerFactory>().As<ILoggerFactory>().SingleInstance();
            builder.RegisterGeneric(typeof(Logger<>)).As(typeof(ILogger<>)).SingleInstance();

            builder.RegisterType<HashCodeCalculator>().As<IHashCodeCalculator>().SingleInstance();
            builder.RegisterType<SystemClock>().As<ISystemClock>().SingleInstance();
        }
    }
}