﻿using System.Web.Http;
using BlueTeam.Web.Api.Common.Filters;
using BlueTeam.Web.Api.Common.Logging;
using BlueTeam.Web.Api.Common.Versioning;
using Newtonsoft.Json.Serialization;

namespace BlueTeam.QuizArt.WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.AddNamespaceBasedApiVersioning();
            config.AddGlobalErrorLogging();

            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            config.Filters.Add(new ValidateModelStateFilter());
        }
    }
}