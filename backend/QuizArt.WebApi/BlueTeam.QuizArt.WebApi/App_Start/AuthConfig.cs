﻿using System;
using System.Web.Http;
using BlueTeam.Common.HashCodeCalculation;
using BlueTeam.Common.SystemClock;
using BlueTeam.QuizArt.WebApi.Providers;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security.OAuth;

namespace BlueTeam.QuizArt.WebApi
{
    public static class AuthConfig
    {
        public static OAuthAuthorizationServerOptions GetOAuthAuthorizationServerOptions(HttpConfiguration config)
        {
            var hashCalculator = (IHashCodeCalculator)config.DependencyResolver.GetService(typeof(IHashCodeCalculator));
            var systemClock = (ISystemClock)config.DependencyResolver.GetService(typeof(ISystemClock));

            var oAuthServerOptions = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = Properties.Settings.Default.AllowInsecureHttp,
                TokenEndpointPath = new PathString(Properties.Settings.Default.TokenEndpointPath),
                AccessTokenExpireTimeSpan = TimeSpan.FromMilliseconds(Properties.Settings.Default.AccessTokenExpireTimeMilliseconds),
                Provider = new CustomOAuthProvider(),
                RefreshTokenProvider = new RefreshTokenProvider(hashCalculator, systemClock),
                AccessTokenFormat = new CustomJwtFormat(Properties.Settings.Default.Issuer)
            };

            return oAuthServerOptions;
        }

        public static JwtBearerAuthenticationOptions GetJwtBearerAuthenticationOptions()
        {
            var issuer = Properties.Settings.Default.Issuer;
            var audienceId = Properties.Settings.Default.AudienceId;
            var audienceSecret = TextEncodings.Base64Url.Decode(Properties.Settings.Default.AudienceSecret);
            var jwtBearerAuthOptions = new JwtBearerAuthenticationOptions
                {
                    AuthenticationMode = AuthenticationMode.Active,
                    AllowedAudiences = new[] {audienceId},
                    IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                    {
                        new SymmetricKeyIssuerSecurityTokenProvider(issuer, audienceSecret)
                    }
                };

            return jwtBearerAuthOptions;
        }
    }
}