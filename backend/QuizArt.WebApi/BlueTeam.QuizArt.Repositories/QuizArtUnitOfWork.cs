﻿using System.Data.Entity;
using BlueTeam.Common.Annotations;
using BlueTeam.QuizArt.DomainModel;
using BlueTeam.QuizArt.Repositories.Interfaces;
using BlueTeam.QuizArt.Repositories.Repositories;
using BlueTeam.Repository.EntityFramework;

namespace BlueTeam.QuizArt.Repositories
{
    [UsedImplicitly]
    public class QuizArtUnitOfWork : UnitOfWork, IQuizArtUnitOfWork
    {
        public ISurveyRepository Surveys => (ISurveyRepository)GetRepository<Survey>();

        public IRefreshTokenRepository RefreshTokens => (IRefreshTokenRepository)GetRepository<RefreshToken>();

        public IUserRepository Users => (IUserRepository)GetRepository<User>();

        public IRoleRepository Roles => (IRoleRepository)GetRepository<Role>();


        public QuizArtUnitOfWork(DbContext context)
            : base(context)
        {
            AddSpecificRepository<Survey, SurveyRepository>();
            AddSpecificRepository<RefreshToken, RefreshTokenRepository>();
            AddSpecificRepository<User, UserRepository>();
            AddSpecificRepository<Role, RoleRepository>();
        }
    }
}