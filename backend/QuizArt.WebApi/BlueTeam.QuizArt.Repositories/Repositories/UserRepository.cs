﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using BlueTeam.Common.Annotations;
using BlueTeam.QuizArt.DomainModel;
using BlueTeam.QuizArt.Repositories.Dto;
using BlueTeam.QuizArt.Repositories.Interfaces;
using BlueTeam.Repository.EntityFramework;

namespace BlueTeam.QuizArt.Repositories.Repositories
{
    [UsedImplicitly]
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(DbContext context)
            : base(context)
        {

        }


        public async Task<User> FindByIdAsync(int userId)
        {
            var user = await GetQuery(u => u.Roles).Where(u => u.Id == userId).SingleOrDefaultAsync();

            return user;
        }

        public async Task<User> FindByUserNameAsync(string userName)
        {
            var user = await GetQuery(u => u.Roles).SingleOrDefaultAsync(u => u.UserName == userName);

            return user;
        }

        public async Task<User> FindByEmailAsync(string email)
        {
            var user = await GetQuery(u => u.Roles).SingleOrDefaultAsync(u => u.Email == email);

            return user;
        }

        public async Task<IReadOnlyCollection<UserWithSurveysCount>> GetUsersWithSurveysCountAsync(int skip, int top)
        {
            var usersWithRolesAndSurveysCount = await Set
                .OrderBy(u => u.JoinDateUtc)
                .Skip(skip)
                .Take(top)
                .Select(u => new UserWithRolesAndSurveysCount
                {
                    User = u,
                    SurveysCount = u.Surveys.Count,
                    Roles = u.Roles
                })
                .ToListAsync();

            var usersWithSurveysCounts = usersWithRolesAndSurveysCount.Select(CreateUserWithSurveysCount).ToList();

            return usersWithSurveysCounts;
        }

        public async Task<UserWithSurveysCount> FindUserWithSurveysCountByUserNameAsync(string userName)
        {
            var usersWithRolesAndSurveysCount = await Set
                .Select(u => new UserWithRolesAndSurveysCount
                {
                    User = u,
                    SurveysCount = u.Surveys.Count,
                    Roles = u.Roles
                })
                .SingleOrDefaultAsync(u => u.User.UserName == userName);

            return CreateUserWithSurveysCount(usersWithRolesAndSurveysCount);
        }


        private static UserWithSurveysCount CreateUserWithSurveysCount(UserWithRolesAndSurveysCount userWithRolesAndSurveysCount)
        {
            var user = userWithRolesAndSurveysCount.User;
            user.Roles = userWithRolesAndSurveysCount.Roles;

            return new UserWithSurveysCount
            {
                User = user,
                SurveysCount = userWithRolesAndSurveysCount.SurveysCount
            };
        }



        private class UserWithRolesAndSurveysCount
        {
            public User User { get; set; }

            public int SurveysCount { get; set; }

            public ICollection<Role> Roles { get; set; }
        }
    }
}