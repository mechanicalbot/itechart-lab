﻿using System.Data.Entity;
using System.Threading.Tasks;
using BlueTeam.Common.Annotations;
using BlueTeam.QuizArt.DomainModel;
using BlueTeam.QuizArt.Repositories.Interfaces;
using BlueTeam.Repository.EntityFramework;

namespace BlueTeam.QuizArt.Repositories.Repositories
{
    [UsedImplicitly]
    public class RoleRepository : Repository<Role>, IRoleRepository
    {
        public RoleRepository(DbContext context) 
            : base(context)
        {

        }


        public async Task<Role> FindByNameAsync(string roleName)
        {
            var role = await Set.SingleOrDefaultAsync(r => r.Name == roleName);

            return role;
        }
    }
}