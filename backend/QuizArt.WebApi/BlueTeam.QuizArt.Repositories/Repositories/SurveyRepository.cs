﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using BlueTeam.Common.Annotations;
using BlueTeam.QuizArt.DomainModel;
using BlueTeam.QuizArt.Repositories.Interfaces;
using BlueTeam.Repository.EntityFramework;

namespace BlueTeam.QuizArt.Repositories.Repositories
{
    [UsedImplicitly]
    public class SurveyRepository : Repository<Survey>, ISurveyRepository
    {
        public SurveyRepository(DbContext context) 
            : base(context)
        {

        }


        public async Task<IReadOnlyCollection<Survey>> GetLatestSurveysAsync(int maxCount)
        {
            var latestSurveys = await Set.OrderByDescending(survey => survey.CreationDate).Take(maxCount).ToListAsync();

            return latestSurveys;
        }
    }
}