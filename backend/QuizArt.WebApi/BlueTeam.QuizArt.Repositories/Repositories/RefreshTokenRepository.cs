﻿using System.Data.Entity;
using System.Threading.Tasks;
using BlueTeam.Common.Annotations;
using BlueTeam.QuizArt.DomainModel;
using BlueTeam.QuizArt.Repositories.Interfaces;
using BlueTeam.Repository.EntityFramework;

namespace BlueTeam.QuizArt.Repositories.Repositories
{
    [UsedImplicitly]
    public class RefreshTokenRepository : Repository<RefreshToken>, IRefreshTokenRepository
    {
        public RefreshTokenRepository(DbContext context) 
            : base(context)
        {

        }


        public async Task<RefreshToken> FindByUserNameAndClientAsync(string userName, string clientId)
        {
            var token = await Set.SingleOrDefaultAsync(r => r.User.UserName == userName && r.ClientId == clientId);

            return token;
        }
    }
}