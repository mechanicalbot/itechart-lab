﻿using System.Data.Entity;
using BlueTeam.Common.Annotations;
using BlueTeam.QuizArt.DomainModel;
using BlueTeam.QuizArt.Repositories.EntitiesConfigurations;
using BlueTeam.QuizArt.Repositories.Migrations;

namespace BlueTeam.QuizArt.Repositories
{
    [UsedImplicitly]
    public class QuizArtDbContext : DbContext
    {
        private const string ConnectionStringName = "QuizArtDatabaseConnection";


        public DbSet<Survey> Surveys { get; set; }

        public DbSet<Client> Clients { get; set; }

        public DbSet<RefreshToken> RefreshTokens { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Role> Roles { get; set; }


        static QuizArtDbContext()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<QuizArtDbContext, QuizArtMigrationConfiguration>());
        }


        public QuizArtDbContext()
            : base(ConnectionStringName)
        {
            Configuration.LazyLoadingEnabled = false;
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ClientConfiguration());
            modelBuilder.Configurations.Add(new RoleConfiguration());
            modelBuilder.Configurations.Add(new UserConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}