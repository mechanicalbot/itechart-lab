using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using BlueTeam.Common.Annotations;
using BlueTeam.QuizArt.DomainModel;
using Microsoft.AspNet.Identity;

namespace BlueTeam.QuizArt.Repositories.Migrations
{
    [UsedImplicitly]
    internal sealed class QuizArtMigrationConfiguration : DbMigrationsConfiguration<QuizArtDbContext>
    {
        public QuizArtMigrationConfiguration()
        {
            AutomaticMigrationsEnabled = false;
        }


        protected override void Seed(QuizArtDbContext context)
        {
            var passwordHasher = new PasswordHasher();
            var adminPasswordHash = passwordHasher.HashPassword("qwerty123");
            var userPasswordHash = passwordHasher.HashPassword("aaa111");

            var userRole = context.Roles.SingleOrDefault(r => r.Name == RoleNames.User) ?? new Role { Name = RoleNames.User };
            var adminRole = context.Roles.SingleOrDefault(r => r.Name == RoleNames.Admin) ?? new Role { Name = RoleNames.Admin };

            context.Roles.AddOrUpdate(r => r.Name, userRole, adminRole);

            var survey1 = context.Surveys.SingleOrDefault(s => s.Name == "Young") ?? new Survey { Name = "Young", CreationDate = DateTime.Now.AddDays(3) };
            var survey2 = context.Surveys.SingleOrDefault(s => s.Name == "Middle") ?? new Survey { Name = "Middle", CreationDate = DateTime.Now.AddDays(2) };
            var survey3 = context.Surveys.SingleOrDefault(s => s.Name == "Old") ?? new Survey { Name = "Old", CreationDate = DateTime.Now };
            var survey4 = context.Surveys.SingleOrDefault(s => s.Name == "Dinosaur") ?? new Survey { Name = "Dinosaur", CreationDate = DateTime.Now.AddDays(-10) };

            context.Surveys.AddOrUpdate(s => s.Name, survey1, survey2, survey3, survey4);

            var simpleUser = new User
            {
                UserName = "user@user.com",
                Email = "user@user.com",
                DisplayName = "Vlad",
                JoinDateUtc = DateTime.UtcNow.AddYears(-2),
                PasswordHash = userPasswordHash,
                Roles = new List<Role> { userRole },
                Surveys = new List<Survey> { survey1, survey3 }
            };
            var adminUser = new User
            {
                UserName = "admin@admin.com",
                Email = "admin@admin.com",
                DisplayName = "Vlad",
                JoinDateUtc = DateTime.UtcNow.AddYears(-3),
                PasswordHash = adminPasswordHash,
                Roles = new List<Role> { userRole, adminRole },
                Surveys = new List<Survey> { survey2, survey4 }
            };

            context.Users.AddOrUpdate(u => u.UserName, adminUser, simpleUser);

            var client = new Client
            {
                Id = "QuizArt_FrontEnd",
                IsActive = true,
                Name = "ReactJS front-end Application",
                RefreshTokenLifeTime = TimeSpan.FromDays(15),
                AllowedOrigin = "http://localhost:8080"
            };

            context.Clients.AddOrUpdate(c => c.Id, client);
        }
    }
}