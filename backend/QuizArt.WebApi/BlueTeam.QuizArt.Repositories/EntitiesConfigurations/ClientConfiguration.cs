﻿using System.Data.Entity.ModelConfiguration;
using BlueTeam.QuizArt.DomainModel;

namespace BlueTeam.QuizArt.Repositories.EntitiesConfigurations
{
    internal class ClientConfiguration : EntityTypeConfiguration<Client>
    {
        public ClientConfiguration()
        {
            Ignore(c => c.RefreshTokenLifeTime);
        }
    }
}