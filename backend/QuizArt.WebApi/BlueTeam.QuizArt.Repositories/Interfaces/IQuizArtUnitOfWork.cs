﻿using BlueTeam.Repository;

namespace BlueTeam.QuizArt.Repositories.Interfaces
{
    public interface IQuizArtUnitOfWork : IUnitOfWork
    {
        ISurveyRepository Surveys { get; }

        IRefreshTokenRepository RefreshTokens { get; }

        IUserRepository Users { get; }

        IRoleRepository Roles { get; }
    }
}