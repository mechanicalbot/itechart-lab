﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BlueTeam.QuizArt.DomainModel;
using BlueTeam.Repository;

namespace BlueTeam.QuizArt.Repositories.Interfaces
{
    public interface ISurveyRepository : IRepository<Survey>
    {
        Task<IReadOnlyCollection<Survey>> GetLatestSurveysAsync(int maxCount);
    }
}