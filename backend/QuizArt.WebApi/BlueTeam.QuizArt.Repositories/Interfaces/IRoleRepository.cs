﻿using System.Threading.Tasks;
using BlueTeam.QuizArt.DomainModel;
using BlueTeam.Repository;

namespace BlueTeam.QuizArt.Repositories.Interfaces
{
    public interface IRoleRepository : IRepository<Role>
    {
        Task<Role> FindByNameAsync(string roleName);
    }
}