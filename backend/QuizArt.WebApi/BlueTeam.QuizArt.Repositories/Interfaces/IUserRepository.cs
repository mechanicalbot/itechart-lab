﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BlueTeam.QuizArt.DomainModel;
using BlueTeam.QuizArt.Repositories.Dto;
using BlueTeam.Repository;

namespace BlueTeam.QuizArt.Repositories.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        Task<User> FindByIdAsync(int userId);

        Task<User> FindByUserNameAsync(string userName);

        Task<User> FindByEmailAsync(string email);

        Task<IReadOnlyCollection<UserWithSurveysCount>> GetUsersWithSurveysCountAsync(int skip, int top);

        Task<UserWithSurveysCount> FindUserWithSurveysCountByUserNameAsync(string userName);
    }
}