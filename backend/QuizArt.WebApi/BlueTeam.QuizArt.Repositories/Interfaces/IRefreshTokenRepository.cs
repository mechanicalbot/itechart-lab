﻿using System.Threading.Tasks;
using BlueTeam.QuizArt.DomainModel;
using BlueTeam.Repository;

namespace BlueTeam.QuizArt.Repositories.Interfaces
{
    public interface IRefreshTokenRepository : IRepository<RefreshToken>
    {
        Task<RefreshToken> FindByUserNameAndClientAsync(string userName, string clientId);
    }
}