﻿using BlueTeam.QuizArt.DomainModel;

namespace BlueTeam.QuizArt.Repositories.Dto
{
    public class UserWithSurveysCount
    {
        public User User { get; set; }

        public int SurveysCount { get; set; }
    }
}
