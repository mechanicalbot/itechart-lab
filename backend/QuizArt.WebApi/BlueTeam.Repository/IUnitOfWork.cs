﻿using System;
using System.Threading.Tasks;

namespace BlueTeam.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        Task SaveAsync();

        IRepository<TEntity> GetRepository<TEntity>() where TEntity : class;
    }
}