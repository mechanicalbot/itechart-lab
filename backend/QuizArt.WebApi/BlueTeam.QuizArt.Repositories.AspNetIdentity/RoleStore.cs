﻿using System;
using System.Threading.Tasks;
using BlueTeam.Common.Annotations;
using BlueTeam.QuizArt.DomainModel;
using BlueTeam.QuizArt.Repositories.Interfaces;
using Microsoft.AspNet.Identity;

namespace BlueTeam.QuizArt.Repositories.AspNetIdentity
{
    [UsedImplicitly]
    public class RoleStore : IRoleStore<Role, int>
    {
        private readonly IQuizArtUnitOfWork _uow;


        public RoleStore(IQuizArtUnitOfWork uow)
        {
            _uow = uow;
        }


        public Task CreateAsync(Role role)
        {
            _uow.Roles.Create(role);

            return _uow.SaveAsync();
        }

        public Task DeleteAsync(Role role)
        {
            throw new NotImplementedException();
        }

        public async Task<Role> FindByIdAsync(int roleId)
        {
            var role = await _uow.Roles.GetAsync(roleId);

            return role;
        }

        public async Task<Role> FindByNameAsync(string roleName)
        {
            var role = await _uow.Roles.FindByNameAsync(roleName);

            return role;
        }

        public Task UpdateAsync(Role role)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}