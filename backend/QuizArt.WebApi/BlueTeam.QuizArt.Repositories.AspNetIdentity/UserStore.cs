﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlueTeam.Common.Annotations;
using BlueTeam.QuizArt.DomainModel;
using BlueTeam.QuizArt.Repositories.Interfaces;
using Microsoft.AspNet.Identity;

namespace BlueTeam.QuizArt.Repositories.AspNetIdentity
{
    [UsedImplicitly]
    public class UserStore : IUserPasswordStore<User, int>, IUserRoleStore<User, int>, IUserEmailStore<User, int>
    {
        private readonly IQuizArtUnitOfWork _uow;


        public UserStore(IQuizArtUnitOfWork uow)
        {
            _uow = uow;
        }


        public Task CreateAsync(User user)
        {
            _uow.Users.Create(user);

            return _uow.SaveAsync();
        }

        public Task UpdateAsync(User user)
        {
            _uow.Users.Update(user);

            return _uow.SaveAsync();
        }

        public Task DeleteAsync(User user)
        {
            _uow.Users.Delete(user);

            return _uow.SaveAsync();
        }

        public async Task<User> FindByIdAsync(int userId)
        {
            var user = await _uow.Users.FindByIdAsync(userId);

            return user;
        }

        public async Task<User> FindByNameAsync(string userName)
        {
            var user = await _uow.Users.FindByUserNameAsync(userName);

            return user;
        }

        public Task SetPasswordHashAsync(User user, string passwordHash)
        {
            user.PasswordHash = passwordHash;

            return Task.FromResult(0);
        }

        public Task<string> GetPasswordHashAsync(User user)
        {
            return Task.FromResult(user.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(User user)
        {
            return Task.FromResult(user.PasswordHash != null);
        }

        public async Task AddToRoleAsync(User user, string roleName)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            if (string.IsNullOrEmpty(roleName))
            {
                throw new ArgumentNullException(nameof(roleName));
            }

            var role = await _uow.Roles.FindByNameAsync(roleName);
            if (role == null)
            {
                throw new KeyNotFoundException(nameof(roleName));
            }
            if (user.Roles == null)
            {
                throw new ArgumentNullException(nameof(user.Roles));
            }
            if (user.Roles.Contains(role))
            {
                return;
            }

            user.Roles.Add(role);
            await _uow.SaveAsync();
        }

        public async Task RemoveFromRoleAsync(User user, string roleName)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            if (string.IsNullOrEmpty(roleName))
            {
                throw new ArgumentNullException(nameof(roleName));
            }

            var role = await _uow.Roles.FindByNameAsync(roleName);
            if (role == null)
            {
                throw new KeyNotFoundException(nameof(roleName));
            }
            if (user.Roles == null)
            {
                throw new ArgumentNullException(nameof(user.Roles));
            }
            if (!user.Roles.Contains(role))
            {
                return;
            }

            user.Roles.Remove(role);
            await _uow.SaveAsync();
        }

        public Task<IList<string>> GetRolesAsync(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            var roles = user.Roles.Select(role => role.Name).ToList();

            return Task.FromResult<IList<string>>(roles);
        }

        public async Task<bool> IsInRoleAsync(User user, string roleName)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            if (string.IsNullOrEmpty(roleName))
            {
                throw new ArgumentNullException(nameof(roleName));
            }

            var role = await _uow.Roles.FindByNameAsync(roleName);

            return user.Roles.Contains(role);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public async Task SetEmailAsync(User user, string email)
        {
            user.Email = email;
            await _uow.SaveAsync();
        }

        public Task<string> GetEmailAsync(User user)
        {
            return Task.FromResult(user.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(User user)
        {
            throw new NotImplementedException();
        }

        public Task SetEmailConfirmedAsync(User user, bool confirmed)
        {
            throw new NotImplementedException();
        }

        public async Task<User> FindByEmailAsync(string email)
        {
            var user = await _uow.Users.FindByEmailAsync(email);

            return user;
        }
    }
}