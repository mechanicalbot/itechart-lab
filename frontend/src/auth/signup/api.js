import { IQUIZART_API_URL } from "/config";
import { httpFetcher, MimeType } from "/utils/http";

const accountsUrl = `${IQUIZART_API_URL}/accounts`;

export const signup = (displayName, email, password, passwordConfirmation) => {
    const data = {
        displayName,
        email,
        password,
        passwordConfirmation
    };
    const json = JSON.stringify(data);

    return httpFetcher.post(accountsUrl, json, { "Content-Type": MimeType.JSON });
};

export const getIsEmailOccupied = (email) =>
    httpFetcher.get(`${accountsUrl}?email=${email}`);