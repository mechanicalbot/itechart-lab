import ServiceResult from "/utils/serviceResult";
import { HttpStatusCode } from "/utils/http";

import { signup, getIsEmailOccupied } from "./api";

export const SignupErrorType = {
    EMAIL_OCCUPIED: "email_occupied",
    INVALID_INPUT: "invalid_input",
    UNKNOWN_ERROR: "unknown_error"
};

export const CheckIsEmailOccupiedErrorType = {
    EMAIL_OCCUPIED: "email_occupied",
    UNKNOWN_ERROR: "unknown_error"
};

export default class SignupService {
    constructor(errorTracker) {
        this.errorTracker = errorTracker;
    }

    signup = async (displayName, email, password, passwordConfirmation) => {
        try {
            const response = await signup(displayName, email, password, passwordConfirmation);

            if (response.ok) {
                return ServiceResult.createSuccess({ displayName, email, password });
            }

            if (response.statusCode === HttpStatusCode.BAD_REQUEST) {
                if (response.data.error === SignupErrorType.EMAIL_OCCUPIED) {
                    return ServiceResult.createUnsuccess(SignupErrorType.EMAIL_OCCUPIED, "E-mail занят");
                } else if (response.data.error === SignupErrorType.INVALID_INPUT) {
                    return ServiceResult.createUnsuccess(SignupErrorType.INVALID_INPUT, "Неверные входные данные");
                } else {
                    return ServiceResult.createUnsuccess(SignupErrorType.UNKNOWN_ERROR, "Произошла ошибка при обработке входных данных");
                }
            }

            this.errorTracker.trackUnsuccessfulResponse(response);
            return ServiceResult.createUnsuccess(SignupErrorType.UNKNOWN_ERROR, "Что-то пошло не так. Пожалуйста, попробуйте позже");
        } catch (error) {
            this.errorTracker.trackError(error);
            return ServiceResult.createUnsuccess(SignupErrorType.UNKNOWN_ERROR, "Что-то пошло не так. Пожалуйста, попробуйте позже");
        }
    }

    checkIsEmailOccupied = async (email) => {
        try {
            const response = await getIsEmailOccupied(email);

            if (response.ok) {
                return response.data.isEmailOccupied
                    ? ServiceResult.createSuccess({ email })
                    : ServiceResult.createUnsuccess(CheckIsEmailOccupiedErrorType.EMAIL_OCCUPIED, "E-mail занят");
            }

            this.errorTracker.trackUnsuccessfulResponse(response);
            return ServiceResult.createUnsuccess(CheckIsEmailOccupiedErrorType.UNKNOWN_ERROR, "При проверке e-mail что-то пошло не так. Пожалуйста, попробуйте позже");
        } catch (error) {
            this.errorTracker.trackError(error);
            return ServiceResult.createUnsuccess(CheckIsEmailOccupiedErrorType.UNKNOWN_ERROR, "При проверке e-mail что-то пошло не так. Пожалуйста, попробуйте позже");
        }
    }
}