import React from "react";
import PropTypes from "prop-types";
import ImmutablePropTypes from "react-immutable-proptypes";

import { validateDisplayName, validateEmail, validatePassword, validatePasswordConfirmation } from "/utils/validators/userCredentials";
import ValidatedForm from "/components/ValidatedForm";

const SignupForm = ({ signup, checkIsEmailOccupied, signupError }) => (
    <ValidatedForm
        header="Регистрация"
        submitButtonText="Регистрация"
        formErrors={[signupError.message]}
        onFormValid={signup}
        formFieldsDescription={[
            {
                type: "text",
                name: "displayName",
                placeholder: "Имя",
                validator: validateDisplayName
            },
            {
                type: "email",
                name: "email",
                placeholder: "E-mail",
                validator: validateEmail,
                onInputLostFocus: checkIsEmailOccupied
            },
            {
                type: "password",
                name: "password",
                placeholder: "Пароль",
                validator: validatePassword
            },
            {
                type: "password",
                name: "passwordConfirmation",
                placeholder: "Подтверждение пароля",
                validator: validatePasswordConfirmation,
                additionalFieldsForValidation: ["password"]
            }
        ]}
    />
);

SignupForm.propTypes = {
    signup: PropTypes.func.isRequired,
    checkIsEmailOccupied: PropTypes.func.isRequired,
    signupError: ImmutablePropTypes.recordOf({
        type: PropTypes.string.isRequired,
        message: PropTypes.string.isRequired
    }).isRequired
};

export default SignupForm;