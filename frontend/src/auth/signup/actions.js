import { createActions, createErrorActions } from "/utils/redux";

import { SIGNUP_START, SIGNUP_SUCCESS, SIGNUP_ERROR, CHECK_IS_EMAIL_OCCUPIED_START, CHECK_IS_EMAIL_OCCUPIED_SUCCESS, CHECK_IS_EMAIL_OCCUPIED_ERROR } from "./actionTypes";
import { loginUser } from "../login/actions";
import SignupService from "./signupService";

export const { signupStart, signupSuccess, checkIsEmailOccupiedStart, checkIsEmailOccupiedSuccess } = createActions({
    [SIGNUP_START]: (displayName, email, password, passwordConfirmation) => ({ displayName, email, password, passwordConfirmation }),

    [SIGNUP_SUCCESS]: () => { },

    [CHECK_IS_EMAIL_OCCUPIED_START]: email => ({ email }),

    [CHECK_IS_EMAIL_OCCUPIED_SUCCESS]: () => { }
});

export const { signupError, checkIsEmailOccupiedError } = createErrorActions({
    [SIGNUP_ERROR]: error => error,

    [CHECK_IS_EMAIL_OCCUPIED_ERROR]: error => error
});

export const signupUser = ({ displayName, email, password, passwordConfirmation }) => async (dispatch, getState, { errorTrackerFactory }) => {
    dispatch(signupStart(displayName, email, password, passwordConfirmation));
    const errorTracker = errorTrackerFactory(getState());
    const service = new SignupService(errorTracker);
    const result = await service.signup(displayName, email, password, passwordConfirmation);

    if (!result.isSuccess) {
        dispatch(signupError(result.error));
        return;
    }

    dispatch(signupSuccess());
    dispatch(loginUser(email, password));
};

export const checkIsEmailOccupied = (email) => async (dispatch, getState, { errorTrackerFactory }) => {
    dispatch(checkIsEmailOccupiedStart(email));
    const errorTracker = errorTrackerFactory(getState());
    const service = new SignupService(errorTracker);
    const result = await service.checkIsEmailOccupied(email);

    if (!result.isSuccess) {
        dispatch(checkIsEmailOccupiedError(result.error));
        return;
    }

    dispatch(checkIsEmailOccupiedSuccess());
};