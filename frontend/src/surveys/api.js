import { IQUIZART_API_URL } from "/config";

export const getSurveys = httpFetcher => (max = 0) => httpFetcher.get(`${IQUIZART_API_URL}/surveys?max=${max}`);