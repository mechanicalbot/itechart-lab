import React from "react";

const CompanyDescription = () => (
    <p><strong>iTechArt Group</strong> – это команда <strong>Remarkable People</strong>, профессионалов в сфере разработки, тестирования, сервисной поддержки
    программных продуктов, модернизации и интеграции бизнес-приложений.</p>
);

export default CompanyDescription;