import React from "react";

import projectLogo from "./projectLogo.png";

const ProjectLogo = () => (
    <img className="about__project-logo" src={projectLogo} alt="Progect logo"></img>
);

export default ProjectLogo;