import React from "react";

const CompanyAddress = () => (
    <address>г. Минск, ул. Толстого 10, БЦ "Титул"</address>
);

export default CompanyAddress;