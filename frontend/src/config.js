//Enabling this option cause access node-specific variable
/*eslint-env node*/

export const APP_VERSION = "v1";
export const ENVIRONMENT_NAME = process.env.NODE_ENV;

export const IQUIZART_URL = "http://localhost:5000";
export const IQUIZART_TOKEN_URL = `${IQUIZART_URL}/oauth/token`;
export const IQUIZART_API_URL = `${IQUIZART_URL}/api/v1`;

export const CLIENT_ID = "QuizArt_FrontEnd";

export const TRACKER_URL = "https://bb91953e56ec459f9cd0b9632014414b@sentry.io/207258";