export const RoleNames = {
    USER: "User",
    ADMIN: "Admin"
};

export const MAIN_ROUTE = "/";
export const ABOUT_ROUTE = "/about";

export const SURVEYS_ROUTE = "/surveys";

export const LOGIN_ROUTE = "/login";
export const LOGOUT_ROUTE = "/logout";
export const SIGNUP_ROUTE = "/signup";
export const FORGET_PASSWORD_ROUTE = "/forget-password";

export const USERS_ROUTE = "/users";

export const INITIAL_ACTION = "INIT";