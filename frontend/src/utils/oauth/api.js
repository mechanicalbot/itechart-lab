import { IQUIZART_TOKEN_URL, CLIENT_ID } from "/config";
import { httpFetcher, MimeType } from "/utils/http";

import { GrantType } from "./constants";

function buildQueryString(object) {
    return Object.keys(object).map(key => `${key}=${encodeURIComponent(object[key])}`).join("&");
}

export const getAccessToken = (email, password) => {
    const body = {
        username: email,
        password,
        grant_type: GrantType.PASSWORD,
        client_id: CLIENT_ID
    };
    const queryString = buildQueryString(body);

    return httpFetcher.post(IQUIZART_TOKEN_URL, queryString, { "Content-Type": MimeType.FORM_ENCODED_DATA });
};

export const refreshAccessToken = (refreshToken) => {
    const body = {
        refresh_token: refreshToken,
        grant_type: GrantType.REFRESH_TOKEN,
        client_id: CLIENT_ID
    };
    const queryString = buildQueryString(body);

    return httpFetcher.post(IQUIZART_TOKEN_URL, queryString, { "Content-Type": MimeType.FORM_ENCODED_DATA });
};