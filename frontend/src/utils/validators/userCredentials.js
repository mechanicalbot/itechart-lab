import validator from "validator";

import ValidationError from "./ValidationError";
import ValidationResult from "./ValidationResult";

const DISPLAY_NAME_MIN_LENGTH = 2;
const PASSWORD_MIN_LENGTH = 6;

const ErrorType = {
    REQUIRED: "required",
    PASSWORD_MATCH: "password_match",
    EMAIL_FORMAT: "email_format",
    EMAIL_OCCUPIED: "email_occupied",
    DISPLAY_NAME_MIN_LENGTH: "display_name_min_length",
    PASSWORD_MIN_LENGTH: "password_min_length"
};

export const validateDisplayName = displayName => {
    const errors = [];
    if (validator.isEmpty(displayName)) {
        const error = new ValidationError(ErrorType.REQUIRED, "Имя необходимо для заполнения");
        errors.push(error);
    }
    if (displayName.length < DISPLAY_NAME_MIN_LENGTH) {
        const error = new ValidationError(ErrorType.DISPLAY_NAME_MIN_LENGTH, `Имя должно состоять как минимум из ${DISPLAY_NAME_MIN_LENGTH} символов`);
        errors.push(error);
    }

    const isValid = errors.length === 0;
    return new ValidationResult(isValid, errors);
};

export const validateEmail = email => {
    const errors = [];
    if (validator.isEmpty(email)) {
        const error = new ValidationError(ErrorType.REQUIRED, "E-mail необходим для заполнения");
        errors.push(error);
    }
    if (!validator.isEmail(email)) {
        const error = new ValidationError(ErrorType.EMAIL_FORMAT, "Неверный формат e-mail");
        errors.push(error);
    }

    const isValid = errors.length === 0;
    return new ValidationResult(isValid, errors);
};

export const validatePassword = password => {
    const errors = [];
    if (validator.isEmpty(password)) {
        const error = new ValidationError(ErrorType.REQUIRED, "Пароль необходим для заполнения");
        errors.push(error);
    }
    if (password.length < PASSWORD_MIN_LENGTH) {
        const error = new ValidationError(ErrorType.PASSWORD_MIN_LENGTH, `Пароль должен состоять как минимум из ${PASSWORD_MIN_LENGTH} символов`);
        errors.push(error);
    }

    const isValid = errors.length === 0;
    return new ValidationResult(isValid, errors);
};

export const validatePasswordConfirmation = (password, passwordConfirmation) => {
    const errors = [];
    if (!validator.equals(password, passwordConfirmation)) {
        const error = new ValidationError(ErrorType.PASSWORD_MATCH, "Пароли должны совпадать");
        errors.push(error);
    }

    const isValid = errors.length === 0;
    return new ValidationResult(isValid, errors);
};