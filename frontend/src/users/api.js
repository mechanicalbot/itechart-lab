import { IQUIZART_API_URL } from "/config";

export const getUsers = httpFetcher => (skip = 0, top = 5) => httpFetcher.get(`${IQUIZART_API_URL}/users?skip=${skip}&top=${top}`);